Задание участникам трэка Android
# Тише едешь - дальше будешь. 
# Developed by Александр Баранов (Telegram: @Alox1d)

Инвест-калькулятор "FinPicker"

### Language and architecture

* Kotlin
* MVVM
* Clean architecture

### Libraries

* AppCompat
* Android KTX
* Material
* Data Binding
* LiveData
* Navigation
* ViewModel
* Kotlin coroutines
* Dagger 2
