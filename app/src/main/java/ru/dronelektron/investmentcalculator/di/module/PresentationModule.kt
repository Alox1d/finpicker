package ru.dronelektron.investmentcalculator.di.module

import androidx.lifecycle.ViewModelProvider
import dagger.Module
import dagger.Provides
import ru.dronelektron.investmentcalculator.di.annotation.InvestForm
import ru.dronelektron.investmentcalculator.domain.repositories.BondsRepository
import ru.dronelektron.investmentcalculator.domain.usecase.CalculateCase
import ru.dronelektron.investmentcalculator.presentation.investform.InvestFormViewModelFactory

@Module
class PresentationModule {
    @Provides
    @InvestForm
    fun provideInvestFormViewModelFactory(
        calculateCase: CalculateCase,
        repo:BondsRepository
    ): ViewModelProvider.Factory = InvestFormViewModelFactory(calculateCase,repo)
}
