package ru.dronelektron.investmentcalculator.di.module

import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import ru.dronelektron.investmentcalculator.data.RetrofitService
import ru.dronelektron.investmentcalculator.domain.repositories.BondsRepository
import ru.dronelektron.investmentcalculator.domain.usecase.CalculateCase

@Module
class DomainModule {
    @Provides
    fun provideBackgroundDispatcher(): CoroutineDispatcher = Dispatchers.Default

    @Provides
    fun provideCalculateProfitUseCase(
        bgDispatcher: CoroutineDispatcher
    ) = CalculateCase(bgDispatcher)
    @Provides
    fun provideMainRepo(
        retro:RetrofitService
    ) = BondsRepository(retrofitService = retro)
    @Provides
    fun provideRetrofit(
    ) = RetrofitService.getInstance()
}
