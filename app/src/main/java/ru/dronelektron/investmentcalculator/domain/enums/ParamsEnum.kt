package ru.dronelektron.investmentcalculator.domain.enums

enum class Wallet(s: String) {
    RUB("Рубль"),
    USD("Доллар")
}
