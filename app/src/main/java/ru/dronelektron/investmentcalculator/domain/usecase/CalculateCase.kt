package ru.dronelektron.investmentcalculator.domain.usecase

import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.withContext
import ru.dronelektron.investmentcalculator.domain.enums.BondEnum
import ru.dronelektron.investmentcalculator.domain.model.CaseResult
import java.text.SimpleDateFormat
import java.util.*
import kotlin.math.pow

class CalculateCase(private val bgDispatcher: CoroutineDispatcher) {
    suspend operator fun invoke(params: Params): CaseResult? = withContext(bgDispatcher) {
        if (params.bolds == null || params.marketBolds == null) return@withContext null
//        val boldsWithPrices = mutableListOf<List<Any>>().also {
//            list ->
//
//            for (marketBold in params.marketBolds){
//                list.add(marketBold[26] as List<Any>)
//            }
//        }
        val pattern = SimpleDateFormat("yyyy-MM-dd")
        val bolds = params.bolds
        val bondNames = mutableListOf<String>()
        val coefs = mutableListOf<Float>()
        var firstBondPercent = 0.25f
        var secondBondPercent = 0.75f

        val boldsPD =
            bolds.filter { (it[BondEnum.SECNAME.i] as String).contains("ОФЗ-ПД") }
//        boldsPD.forEach { it.sortedBy { (it[BondEnum.MATDATE.i] as String) } }
//        val sortedPD = boldsPD.sortedBy { it[BondEnum.MATDATE.i] as String }
        var minDifInDates = -1
        var closestBond: List<Any>? = null
        val boldsClosestBefore = boldsPD.filter {
            val date = it[BondEnum.MATDATE.i] as String
            val calBold = Calendar.getInstance()
            calBold.setTime(pattern.parse(date))

            val calEnd = Calendar.getInstance()
            calEnd.add(Calendar.MONTH, params.durationInMonths)
//            val months: Long = ChronoUnit.MONTHS.between(calBold.toInstant(), calEnd.toInstant())
//            val months = Period(calBold.getTimeInMillis(), calEnd.getTimeInMillis()).months
            val seconds: Long = (calEnd.getTimeInMillis() - calBold.getTimeInMillis()) / 1000
            seconds >= 0
        }
        var bestBondBefore: BondBefore? = null
        if (params.durationInMonths > 12) {


            if (!boldsClosestBefore.isNullOrEmpty()) {
                //Поиск лучшей облигации по доходности к погашению
                var maxRepayPercent = -1.0
                for (bond in boldsClosestBefore) {
                    val bondData = generateBondByRepayment(bond, params.marketBolds)
                    if (bondData.repaymentIncomePercent != 0.0 &&
                        bondData.repaymentIncomePercent > maxRepayPercent
                    ) {
                        bestBondBefore = bondData
                        maxRepayPercent = bondData.repaymentIncomePercent
                    }
                }
                bondNames.add(bestBondBefore?.bond?.get(2) as String)
                coefs.add(firstBondPercent)

            }
        } else {
            firstBondPercent = 0.0f
            secondBondPercent = 1.0f

        }
        val bondsClosestAfter = boldsPD.filter {
            val date = it[BondEnum.MATDATE.i] as String
            val calBond = Calendar.getInstance()
            calBond.setTime(pattern.parse(date))

            val calEnd = Calendar.getInstance()
            calEnd.add(Calendar.MONTH, params.durationInMonths)
//            val months: Long = ChronoUnit.MONTHS.between(calBold.toInstant(), calEnd.toInstant())
//            val months = Period(calBold.getTimeInMillis(), calEnd.getTimeInMillis()).months
            val seconds: Long = (calBond.getTimeInMillis() - calEnd.getTimeInMillis()) / 1000
            seconds >= 0
        }

        //Поиск лучшей облигации по текущей доходности
        var maxCurrentPercent = -1.0
        var bestBondAfter: BondAfter? = null
        for (bond in bondsClosestAfter) {
            val bondData = generateBondByCurrent(bond, params.marketBolds, params.durationInMonths)
            if (bondData.currentIncomePercent != 0.0 &&
                bondData.currentIncomePercent > maxCurrentPercent
            ) {
                bestBondAfter = bondData
                maxCurrentPercent = bondData.currentIncomePercent
            }
        }
        bondNames.add(0, bestBondAfter?.bond?.get(2) as String)
        coefs.add(secondBondPercent)

//        var max1CurrentIncomePercent = -1.0
//        var max2CurrentIncomePercent = -1.0
//        var best1LocalBondData: LocalBondData? = null
//        var best2LocalBondData: LocalBondData? = null
//        for (bold in boldsPD) {
//            val boldData = generateBoldByRepayment(bold, params.marketBolds,params.durationInMonths)
//            if (boldData.currentIncomePercent > max1CurrentIncomePercent) {
//                best2LocalBondData = best1LocalBondData
//                best1LocalBondData = boldData
//                max2CurrentIncomePercent = max1CurrentIncomePercent
//                max1CurrentIncomePercent = boldData.currentIncomePercent
//            } else if (boldData.currentIncomePercent > max2CurrentIncomePercent) {
//                best2LocalBondData = boldData
//                max2CurrentIncomePercent = boldData.currentIncomePercent
//
//            }
//        }

        var result = 0.0
        if (bestBondBefore != null) {
            val lotSize1 = bestBondBefore.bond[9] as Double
            val nkd1 = bestBondBefore.bond[7] as Double
            val countBondBefore =
                Math.floor(params.sum * (1 - secondBondPercent) / ((bestBondBefore.marketPrice + nkd1) * lotSize1))
                    .toInt()
            val couponPrice1 = (bestBondBefore.bond[BondEnum.COUPONVALUE.i] as Double)
            val nominal1 = (bestBondBefore.bond[BondEnum.FACEVALUE.i] as Double)
            //TODO Маленькая сумма
            result = countBondBefore * (maxOf(
                nominal1,
                bestBondBefore.marketPrice
            ) + (couponPrice1 * 0.87 * bestBondBefore.coupons))
        }
        val lotSize2 = bestBondAfter.bond[9] as Double
        val nkd2 = bestBondAfter.bond[7] as Double
        val countBondAfter =
            Math.floor(params.sum * (1 - firstBondPercent) / ((bestBondAfter.marketPrice + nkd2) * lotSize2))
                .toInt()
//        val durationInDays = params.durationInMonths * 30
//        val receivedCoupons1 =
//            Math.ceil(durationInDays / (best1LocalBondData.bold[BondEnum.COUPONPERIOD.i] as Double))
//        val receivedCoupons2 =
//            Math.ceil(durationInDays / (best2LocalBondData.bold[BondEnum.COUPONPERIOD.i] as Double))
        val couponPrice2 = (bestBondAfter.bond[BondEnum.COUPONVALUE.i] as Double)
        val nominal2 = (bestBondAfter.bond[BondEnum.FACEVALUE.i] as Double)
        val incomeBondAfter = countBondAfter * (maxOf(
            nominal2,
            bestBondAfter.marketPrice
        ) + (couponPrice2 * 0.87 * bestBondAfter.coupons))
        result += incomeBondAfter

        val years = Math.floor(params.durationInMonths / 12.0)
        val incomeYearPercent = (result / (params.sum)).pow(1 / years) - 1

        var sumRegular = 0.0
//        if(params.durationInMonths<=12){
            var sumMontly = 0.0
            for (month in 1..params.durationInMonths) {
                sumMontly += params.sumAddMontly

                val bondPrice = (bestBondAfter.marketPrice + nkd2) * lotSize2
                if (sumMontly > bondPrice) {
                    val count = Math.floor(sumMontly/bondPrice)
                    val calEnd = Calendar.getInstance()
                    calEnd.add(Calendar.MONTH,params.durationInMonths)
                    val calNow = Calendar.getInstance()
                    calNow.add(Calendar.MONTH,month)
                    val seconds: Long = (calEnd.getTimeInMillis() - calNow.getTimeInMillis()) / 1000
                    val days = Math.ceil(((seconds / 3600) / 24.0)).toInt()
                    val coupons =
                        Math.ceil((days / (bestBondAfter.bond[BondEnum.COUPONPERIOD.i] as Double))).toInt()
                    val sumCoupons = (couponPrice2 * 0.87 * coupons)
                    val max = maxOf(
                        nominal2,
                        bestBondAfter.marketPrice
                    )
                    val bond =  max + sumCoupons
                    val refillProfit =count * bond

                    sumMontly -= bondPrice*count
                    result += refillProfit
                    sumRegular += bondPrice*count

                }
            }
//        }


        if (params.isIIS) {
            val sumYear = params.sum
            var addedByIIS = 0.0
            //TODO Учёт ежемесячных платежей
//            if (params.sum<1000_000)

//            for (year in 1..years) {
//                if (sumOutYear<400_000)
//                addedByIIS += sumOutYear * 0.13
//                else
//                addedByIIS += 400_000 * 0.13
//
//            }
            if (sumYear < 400_000)
                addedByIIS += sumYear * 0.13
            else
                addedByIIS += 400_000 * 0.13
            result += addedByIIS
        }


        val profit = result - params.sum - sumRegular

        return@withContext CaseResult(
            result,
            incomeYearPercent,
            profit,
            bondNames.toTypedArray(),
            coefs.toTypedArray().toFloatArray()
        )
//        mutableListOf<Profit>().also { list ->
//            for (iteration in 1..params.iterations) {
//                val multiplier = getMultiplier(params.percent, iteration)
//                val profitBalance = params.balance * multiplier
//                val profitPercent = (profitBalance - params.balance) / params.balance * 100.0
//
//                list.add(Profit(iteration, profitBalance, profitPercent))
//            }
//        }
    }

    private fun generateBondByRepayment(
        bond: List<Any>,
        marketBolds: List<List<Any>>

    ): BondBefore {
//        val period = bond[BondEnum.COUPONPERIOD.i] as Double
        val coupon = bond[BondEnum.COUPONVALUE.i] as Double
//        val couponYear = (coupon / period) * 365

        val bondSecID = bond[0] as String
        val marketBold = marketBolds.find { (it[0] as String).contains(bondSecID) }
        val marketPricePercentS = bond[BondEnum.PREVPRICE.i] as? String ?: ""
        var marketPricePercent = marketBold?.get(27) as? Double ?: 100.0
//        marketBold?.forEachIndexed { index, any -> if(index==26) marketPricePercent = any as Dou }
        val nominal = (bond[10] as Double)
//        for (i in bond.indices){
//            val item = bond[i]
//            if (i==8){
//                marketPricePercent = bond[i] as Double
//            }
//        }
//        bond.forEach {
//            val item = it
//            val item2 = it
//        }
        val marketPrice = (nominal / 100) * marketPricePercent

        val date = bond[BondEnum.MATDATE.i] as String
        val pattern = SimpleDateFormat("yyyy-MM-dd")
        val calBold = Calendar.getInstance()
        calBold.setTime(pattern.parse(date))

        val calNow = Calendar.getInstance()
        val seconds: Long = (calBold.getTimeInMillis() - calNow.getTimeInMillis()) / 1000
        val days = Math.ceil(((seconds / 3600) / 24.0)).toInt()
        val coupons =
            Math.ceil((days / (bond[BondEnum.COUPONPERIOD.i] as Double))).toInt()
        val nkd = bond[7] as Double
        val s = (nominal - marketPrice + coupons * coupon - nkd) / marketPrice
        val d = (365.0 / days)
        val repaymentIncomePercent = s * d * 0.87

        return BondBefore(
            bond,
            repaymentIncomePercent,
            marketPrice,
            coupons
        )
    }

//    private fun generateLocalBondDataByAssert(
//        bond: List<Any>,
//        marketBolds: List<List<Any>>
//
//    ): BondBefore {
////        val period = bond[BondEnum.COUPONPERIOD.i] as Double
////        val coupon = bond[BondEnum.COUPONVALUE.i] as Double
////        val couponYear = (coupon / period) * 365
//
//        val bondSecID = bond[0] as String
//        val marketBold = marketBolds.find { (it[0] as String).contains(bondSecID) }
//        val marketPricePercent = (marketBold?.get(26) as? Double) ?: 0.0
//        val nominal = (bond[10] as Double)
//        val marketPrice = (nominal / 100) * marketPricePercent
//
//        val date = bond[BondEnum.MATDATE.i] as String
//        val pattern = SimpleDateFormat("yyyy-MM-dd")
//        val calBold = Calendar.getInstance()
//        calBold.setTime(pattern.parse(date))
//
//        val calNow = Calendar.getInstance()
//        val seconds: Long = (calBold.getTimeInMillis() - calNow.getTimeInMillis()) / 1000
//        val days = ((seconds / 3600) / 24).toInt()
//        val coupons =
//            Math.ceil(days / (bond[BondEnum.COUPONPERIOD.i] as Double))
//        val nkd = bond[7] as Double
//        val repaymentIncomePercent =
//            if (marketPricePercent != 0.0)
////                    (couponYear / marketPrice) * 0.87
//                ((nominal - marketPrice + (coupons*
//                    (bond[BondEnum.COUPONVALUE.i] as Double)) - nkd) / marketPrice) * (365 / days)
//            else 0.0
//        return BondBefore(
//            bond,
//            repaymentIncomePercent,
//            marketPrice
//        )
//    }

    private fun generateBondByCurrent(
        bond: List<Any>,
        marketBolds: List<List<Any>>,
        durationInMonths: Int

    ): BondAfter {
        val period = bond[BondEnum.COUPONPERIOD.i] as Double
        val coupon = bond[BondEnum.COUPONVALUE.i] as Double
        val couponYear = (coupon / period) * 365

        val bondSecID = bond[0] as String
        val marketBold = marketBolds.find { (it[0] as String).contains(bondSecID) }
        val marketPricePercent = bond[BondEnum.PREVPRICE.i] as? Double ?: 100.0
//        val marketPricePercent = (marketBold?.get(26) as? Double) ?: 0.0
        val nominal = (bond[10] as Double)
        val marketPrice = (nominal / 100) * marketPricePercent


//        val date = bond[BondEnum.MATDATE.i] as String
//        val pattern = SimpleDateFormat("yyyy-MM-dd")
//        val calBond = Calendar.getInstance()
//        calBond.setTime(pattern.parse(date))

        val calEnd = Calendar.getInstance()
        val calNow = Calendar.getInstance()
        calEnd.add(Calendar.MONTH, durationInMonths)

        val seconds: Long = (calEnd.getTimeInMillis() - calNow.getTimeInMillis()) / 1000
        val days = Math.ceil((seconds / 3600) / 24.0).toInt()
        val coupons =
            Math.ceil((days / (bond[BondEnum.COUPONPERIOD.i] as Double))).toInt()

        val currentIncomePercent =
            if (marketPricePercent != 0.0) (couponYear / marketPrice) * 0.87 else 0.0
        return BondAfter(
            bond,
            currentIncomePercent,
            marketPrice,
            coupons
        )
    }

    data class BondBefore(
        val bond: List<Any>,
        val repaymentIncomePercent: Double,
        val marketPrice: Double,
        val coupons: Int
    )

    data class BondAfter(
        val bond: List<Any>,
        val currentIncomePercent: Double,
        val marketPrice: Double,
        val coupons: Int

    )

    data class Params(
        val bolds: List<List<Any>>?,
        val marketBolds: List<List<Any>>?,
        val sum: Double,
        val durationInMonths: Int,
        val isIIS: Boolean,
        val wallet: String,
        val sumAddMontly: Double
    )
}
