//package ru.dronelektron.investmentcalculator.domain.usecase
//
//import kotlinx.coroutines.withContext
//import ru.dronelektron.investmentcalculator.domain.model.Profit
//
//class CalculateCaseOld {
//    suspend operator fun invoke(params: CalculateCaseOld.Params): List<Profit> = withContext(bgDispatcher) {
//        mutableListOf<Profit>().also { list ->
//            for (iteration in 1..params.iterations) {
//                val multiplier = getMultiplier(params.percent, iteration)
//                val profitBalance = params.balance * multiplier
//                val profitPercent = (profitBalance - params.balance) / params.balance * 100.0
//
//                list.add(Profit(iteration, profitBalance, profitPercent))
//            }
//        }
//    }
//    data class Params(
//
//    )
//}