package ru.dronelektron.investmentcalculator.domain.model

data class BondCollection(
    val dataversion: Dataversion,
    val marketdata: Marketdata,
    val marketdata_yields: MarketdataYields,
    val securities: Securities
)

data class Dataversion(
    val columns: List<String>,
    val `data`: List<List<Int>>,
    val metadata: Metadata
)

data class Marketdata(
    val columns: List<String>,
    val `data`: List<List<Any>>,
    val metadata: MetadataX
)

data class MarketdataYields(
    val columns: List<String>,
    val `data`: List<List<Any>>,
    val metadata: MetadataXX
)

data class Securities(
    val columns: List<String>,
    val data: List<List<Any>>,
    val metadata: MetadataXXX
)

data class Metadata(
    val data_version: DataVersionX,
    val seqnum: Seqnum
)

data class DataVersionX(
    val type: String
)

data class Seqnum(
    val type: String
)

data class MetadataX(
    val ADMITTEDQUOTE: ADMITTEDQUOTE,
    val BEICLOSE: BEICLOSE,
    val BID: BID,
    val BIDDEPTH: BIDDEPTH,
    val BIDDEPTHT: BIDDEPTHT,
    val BOARDID: BOARDID,
    val CBRCLOSE: CBRCLOSE,
    val CHANGE: CHANGE,
    val CLOSEPRICE: CLOSEPRICE,
    val CLOSEYIELD: CLOSEYIELD,
    val DURATION: DURATION,
    val HIGH: HIGH,
    val HIGHBID: HIGHBID,
    val IRICPICLOSE: IRICPICLOSE,
    val LAST: LAST,
    val LASTBID: LASTBID,
    val LASTCHANGE: LASTCHANGE,
    val LASTCHANGEPRCNT: LASTCHANGEPRCNT,
    val LASTCNGTOLASTWAPRICE: LASTCNGTOLASTWAPRICE,
    val LASTOFFER: LASTOFFER,
    val LASTTOPREVPRICE: LASTTOPREVPRICE,
    val LCLOSEPRICE: LCLOSEPRICE,
    val LCURRENTPRICE: LCURRENTPRICE,
    val LOW: LOW,
    val LOWOFFER: LOWOFFER,
    val MARKETPRICE: MARKETPRICE,
    val MARKETPRICE2: MARKETPRICE2,
    val MARKETPRICETODAY: MARKETPRICETODAY,
    val NUMBIDS: NUMBIDS,
    val NUMOFFERS: NUMOFFERS,
    val NUMTRADES: NUMTRADES,
    val OFFER: OFFER,
    val OFFERDEPTH: OFFERDEPTH,
    val OFFERDEPTHT: OFFERDEPTHT,
    val OPEN: OPEN,
    val OPENPERIODPRICE: OPENPERIODPRICE,
    val PRICEMINUSPREVWAPRICE: PRICEMINUSPREVWAPRICE,
    val QTY: QTY,
    val SECID: SECID,
    val SEQNUM: SEQNUMX,
    val SPREAD: SPREAD,
    val SYSTIME: SYSTIME,
    val TIME: TIME,
    val TRADINGSESSION: TRADINGSESSION,
    val TRADINGSTATUS: TRADINGSTATUS,
    val UPDATETIME: UPDATETIME,
    val VALTODAY: VALTODAY,
    val VALTODAY_RUR: VALTODAYRUR,
    val VALTODAY_USD: VALTODAYUSD,
    val VALUE: VALUE,
    val VALUE_USD: VALUEUSD,
    val VOLTODAY: VOLTODAY,
    val WAPRICE: WAPRICE,
    val WAPTOPREVWAPRICE: WAPTOPREVWAPRICE,
    val WAPTOPREVWAPRICEPRCNT: WAPTOPREVWAPRICEPRCNT,
    val YIELD: YIELD,
    val YIELDATWAPRICE: YIELDATWAPRICE,
    val YIELDLASTCOUPON: YIELDLASTCOUPON,
    val YIELDTOOFFER: YIELDTOOFFER,
    val YIELDTOPREVYIELD: YIELDTOPREVYIELD
)

data class ADMITTEDQUOTE(
    val type: String
)

data class BEICLOSE(
    val type: String
)

data class BID(
    val type: String
)

data class BIDDEPTH(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class BIDDEPTHT(
    val type: String
)

data class BOARDID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class CBRCLOSE(
    val type: String
)

data class CHANGE(
    val type: String
)

data class CLOSEPRICE(
    val type: String
)

data class CLOSEYIELD(
    val type: String
)

data class DURATION(
    val type: String
)

data class HIGH(
    val type: String
)

data class HIGHBID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class IRICPICLOSE(
    val type: String
)

data class LAST(
    val type: String
)

data class LASTBID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class LASTCHANGE(
    val type: String
)

data class LASTCHANGEPRCNT(
    val type: String
)

data class LASTCNGTOLASTWAPRICE(
    val type: String
)

data class LASTOFFER(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class LASTTOPREVPRICE(
    val type: String
)

data class LCLOSEPRICE(
    val type: String
)

data class LCURRENTPRICE(
    val type: String
)

data class LOW(
    val type: String
)

data class LOWOFFER(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class MARKETPRICE(
    val type: String
)

data class MARKETPRICE2(
    val type: String
)

data class MARKETPRICETODAY(
    val type: String
)

data class NUMBIDS(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class NUMOFFERS(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class NUMTRADES(
    val type: String
)

data class OFFER(
    val type: String
)

data class OFFERDEPTH(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class OFFERDEPTHT(
    val type: String
)

data class OPEN(
    val type: String
)

data class OPENPERIODPRICE(
    val type: String
)

data class PRICEMINUSPREVWAPRICE(
    val type: String
)

data class QTY(
    val type: String
)

data class SECID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SEQNUMX(
    val type: String
)

data class SPREAD(
    val type: String
)

data class SYSTIME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class TIME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class TRADINGSESSION(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class TRADINGSTATUS(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class UPDATETIME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class VALTODAY(
    val type: String
)

data class VALTODAYRUR(
    val type: String
)

data class VALTODAYUSD(
    val type: String
)

data class VALUE(
    val type: String
)

data class VALUEUSD(
    val type: String
)

data class VOLTODAY(
    val type: String
)

data class WAPRICE(
    val type: String
)

data class WAPTOPREVWAPRICE(
    val type: String
)

data class WAPTOPREVWAPRICEPRCNT(
    val type: String
)

data class YIELD(
    val type: String
)

data class YIELDATWAPRICE(
    val type: String
)

data class YIELDLASTCOUPON(
    val type: String
)

data class YIELDTOOFFER(
    val type: String
)

data class YIELDTOPREVYIELD(
    val type: String
)

data class MetadataXX(
    val BEI: BEI,
    val BOARDID: BOARDIDX,
    val CBR: CBR,
    val DURATION: DURATIONX,
    val DURATIONWAPRICE: DURATIONWAPRICE,
    val EFFECTIVEYIELD: EFFECTIVEYIELD,
    val EFFECTIVEYIELDWAPRICE: EFFECTIVEYIELDWAPRICE,
    val GSPREADBP: GSPREADBP,
    val ICPI: ICPI,
    val IR: IR,
    val PRICE: PRICE,
    val SECID: SECIDX,
    val SEQNUM: SEQNUMXX,
    val SYSTIME: SYSTIMEX,
    val TRADEMOMENT: TRADEMOMENT,
    val WAPRICE: WAPRICEX,
    val YIELDDATE: YIELDDATE,
    val YIELDDATETYPE: YIELDDATETYPE,
    val YIELDLASTCOUPON: YIELDLASTCOUPONX,
    val YIELDTOOFFER: YIELDTOOFFERX,
    val ZCYCMOMENT: ZCYCMOMENT,
    val ZSPREADBP: ZSPREADBP
)

data class BEI(
    val type: String
)

data class BOARDIDX(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class CBR(
    val type: String
)

data class DURATIONX(
    val type: String
)

data class DURATIONWAPRICE(
    val type: String
)

data class EFFECTIVEYIELD(
    val type: String
)

data class EFFECTIVEYIELDWAPRICE(
    val type: String
)

data class GSPREADBP(
    val type: String
)

data class ICPI(
    val type: String
)

data class IR(
    val type: String
)

data class PRICE(
    val type: String
)

data class SECIDX(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SEQNUMXX(
    val type: String
)

data class SYSTIMEX(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class TRADEMOMENT(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class WAPRICEX(
    val type: String
)

data class YIELDDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class YIELDDATETYPE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class YIELDLASTCOUPONX(
    val type: String
)

data class YIELDTOOFFERX(
    val type: String
)

data class ZCYCMOMENT(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class ZSPREADBP(
    val type: String
)

data class MetadataXXX(
    val ACCRUEDINT: ACCRUEDINT,
    val BOARDID: BOARDIDXX,
    val BOARDNAME: BOARDNAME,
    val BUYBACKDATE: BUYBACKDATE,
    val BUYBACKPRICE: BUYBACKPRICE,
    val COUPONPERCENT: COUPONPERCENT,
    val COUPONPERIOD: COUPONPERIOD,
    val COUPONVALUE: COUPONVALUE,
    val CURRENCYID: CURRENCYID,
    val DECIMALS: DECIMALS,
    val FACEUNIT: FACEUNIT,
    val FACEVALUE: FACEVALUE,
    val INSTRID: INSTRID,
    val ISIN: ISIN,
    val ISSUESIZE: ISSUESIZE,
    val ISSUESIZEPLACED: ISSUESIZEPLACED,
    val LATNAME: LATNAME,
    val LISTLEVEL: LISTLEVEL,
    val LOTSIZE: LOTSIZE,
    val LOTVALUE: LOTVALUE,
    val MARKETCODE: MARKETCODE,
    val MATDATE: MATDATE,
    val MINSTEP: MINSTEP,
    val NEXTCOUPON: NEXTCOUPON,
    val OFFERDATE: OFFERDATE,
    val PREVADMITTEDQUOTE: PREVADMITTEDQUOTE,
    val PREVDATE: PREVDATE,
    val PREVLEGALCLOSEPRICE: PREVLEGALCLOSEPRICE,
    val PREVPRICE: PREVPRICE,
    val PREVWAPRICE: PREVWAPRICE,
    val REGNUMBER: REGNUMBER,
    val REMARKS: REMARKS,
    val SECID: SECIDXX,
    val SECNAME: SECNAME,
    val SECTORID: SECTORID,
    val SECTYPE: SECTYPE,
    val SETTLEDATE: SETTLEDATE,
    val SHORTNAME: SHORTNAME,
    val STATUS: STATUS,
    val YIELDATPREVWAPRICE: YIELDATPREVWAPRICE
)

data class ACCRUEDINT(
    val type: String
)

data class BOARDIDXX(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class BOARDNAME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class BUYBACKDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class BUYBACKPRICE(
    val type: String
)

data class COUPONPERCENT(
    val type: String
)

data class COUPONPERIOD(
    val type: String
)

data class COUPONVALUE(
    val type: String
)

data class CURRENCYID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class DECIMALS(
    val type: String
)

data class FACEUNIT(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class FACEVALUE(
    val type: String
)

data class INSTRID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class ISIN(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class ISSUESIZE(
    val type: String
)

data class ISSUESIZEPLACED(
    val type: String
)

data class LATNAME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class LISTLEVEL(
    val type: String
)

data class LOTSIZE(
    val type: String
)

data class LOTVALUE(
    val type: String
)

data class MARKETCODE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class MATDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class MINSTEP(
    val type: String
)

data class NEXTCOUPON(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class OFFERDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class PREVADMITTEDQUOTE(
    val type: String
)

data class PREVDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class PREVLEGALCLOSEPRICE(
    val type: String
)

data class PREVPRICE(
    val type: String
)

data class PREVWAPRICE(
    val type: String
)

data class REGNUMBER(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class REMARKS(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SECIDXX(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SECNAME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SECTORID(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SECTYPE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SETTLEDATE(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class SHORTNAME(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class STATUS(
    val bytes: Int,
    val max_size: Int,
    val type: String
)

data class YIELDATPREVWAPRICE(
    val type: String
)