package ru.dronelektron.investmentcalculator.domain.enums

enum class FieldError { EMPTY_FIELD, ZERO_FIELD }
