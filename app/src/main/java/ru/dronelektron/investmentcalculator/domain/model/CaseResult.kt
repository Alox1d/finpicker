package ru.dronelektron.investmentcalculator.domain.model

data class CaseResult(
    val sumOut:Double,
    val incomePercent:Double,
    val profit: Double,
    val bondNames: Array<String>,
    val bondValues: FloatArray,

)
