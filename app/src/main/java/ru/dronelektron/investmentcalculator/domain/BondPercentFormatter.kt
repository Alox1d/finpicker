package ru.dronelektron.investmentcalculator.domain

import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.ValueFormatter
import ru.dronelektron.investmentcalculator.presentation.roundTo
import java.text.DecimalFormat
import kotlin.math.roundToInt


class BondPercentFormatter : ValueFormatter() {
    private val format = DecimalFormat("###,###0")

    // override this for BarChart
    override fun getBarLabel(barEntry: BarEntry?): String {
        return format.format(barEntry?.y)
    }

    override fun getFormattedValue(value: Float): String {
        return format.format((value.toDouble()*100).roundTo(0)) + " %"
    }
}