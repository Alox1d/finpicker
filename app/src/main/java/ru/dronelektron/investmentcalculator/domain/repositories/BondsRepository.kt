package ru.dronelektron.investmentcalculator.domain.repositories

import ru.dronelektron.investmentcalculator.data.RetrofitService

class BondsRepository constructor(private val retrofitService: RetrofitService) {

    fun getAllBonds() = retrofitService.getAllSecurities()
}