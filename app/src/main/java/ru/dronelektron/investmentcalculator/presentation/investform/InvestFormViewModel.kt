package ru.dronelektron.investmentcalculator.presentation.investform

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import ru.dronelektron.investmentcalculator.domain.enums.FieldError
import ru.dronelektron.investmentcalculator.domain.enums.Wallet
import ru.dronelektron.investmentcalculator.domain.model.BondCollection
import ru.dronelektron.investmentcalculator.domain.model.CaseResult
import ru.dronelektron.investmentcalculator.domain.model.Profit
import ru.dronelektron.investmentcalculator.domain.repositories.BondsRepository
import ru.dronelektron.investmentcalculator.domain.usecase.CalculateCase
import ru.dronelektron.investmentcalculator.domain.validator.DoubleValidator
import ru.dronelektron.investmentcalculator.domain.validator.IntValidator
import ru.dronelektron.investmentcalculator.presentation.Event
import ru.dronelektron.investmentcalculator.presentation.roundTo

class InvestFormViewModel(
    private val calculateCase: CalculateCase,
    private val repository: BondsRepository
) : ViewModel() {
    private val _profits = MutableLiveData<List<Profit>>()
    private val _isResultsLoading = MutableLiveData<Boolean>()

    private val _sumError = MutableLiveData<FieldError>()
    private val _percentError = MutableLiveData<FieldError>()
    private val _durationError = MutableLiveData<FieldError>()
    private val _navigateToResultsEvent = MutableLiveData<Event<Unit>>()

    val sum = MutableLiveData<String>()
    val incomePercent = MutableLiveData<String>() // Доходность
    val sumAddMontly = MutableLiveData<String>()
    val wallet = MutableLiveData<String>()
    val duration = MutableLiveData<String>()
    val isIIS: MutableLiveData<Boolean> = MutableLiveData()
    val sumOut = MutableLiveData<String>()
    val profit = MutableLiveData<String>()
    val caseResult: MutableLiveData<CaseResult> = MutableLiveData()

    val wallets = MutableLiveData<List<String>>()
    val percent = MutableLiveData<String>()
    val profits: LiveData<List<Profit>> = _profits
    val isProfitsLoading: LiveData<Boolean> = _isResultsLoading
    val sumError: LiveData<FieldError> = _sumError

    //    val percentError: LiveData<FieldError> = _percentError
    val durationError: LiveData<FieldError> = _durationError
    val navigateToResultsEvent: LiveData<Event<Unit>> = _navigateToResultsEvent

    init {
        //Testing
        sum.postValue("100000")
        duration.postValue("24")
        sumAddMontly.postValue("0")

        wallet.postValue(Wallet.RUB.name)
        isIIS.postValue(false)
        wallets.postValue(enumValues<Wallet>().map { it.name })
    }

    fun makeCase() {
        _sumError.value = DoubleValidator.check(sum.value)
//        _percentError.value = DoubleValidator.check(percent.value)
        _durationError.value = IntValidator.check(duration.value)

        if (_sumError.value != null) return
//        if (_percentError.value != null) return
        if (_durationError.value != null) return

        val sumValue = DoubleValidator.transform(sum.value)
//        val percentValue = DoubleValidator.transform(percent.value)
        val durationValue = IntValidator.transform(duration.value)

//        val params = CalculateProfitUseCase.Params(sumValue, durationValue)
        viewModelScope.launch {
            _profits.value = null
            _isResultsLoading.value = true
            runCalculation()

            _isResultsLoading.value = false
        }

        _navigateToResultsEvent.value = Event(Unit)
    }

    val bondList = MutableLiveData<BondCollection>()
    val errorMessage = MutableLiveData<String>()

    suspend fun runCalculation() {

        val response = repository.getAllBonds()
        response.enqueue(object : Callback<BondCollection> {
            override fun onResponse(
                call: Call<BondCollection>,
                response: Response<BondCollection>
            ) {
                bondList.postValue(response.body())
                formResult(response.body())
            }

            override fun onFailure(call: Call<BondCollection>, t: Throwable) {
                errorMessage.postValue(t.message)
            }
        })
    }

     fun formResult(body: BondCollection?) {
        val params = CalculateCase.Params(
            body?.securities?.data,
            body?.marketdata?.data,
            sum.value!!.toDouble(),
            duration.value!!.toInt(),
            isIIS.value!!,
            wallet.value!!,
            sumAddMontly.value!!.toDouble()
        )
         viewModelScope.launch {

         caseResult.value = calculateCase(params)
        profit.postValue(caseResult.value!!.profit.roundTo(2).toString())
        sumOut.postValue(caseResult.value!!.sumOut.roundTo(2).toString())
        incomePercent.postValue(((caseResult.value!!.incomePercent*100).roundTo(2)).toString()+"%")
        }
    }
}
