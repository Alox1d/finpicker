package ru.dronelektron.investmentcalculator.presentation.investform

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import ru.dronelektron.investmentcalculator.domain.repositories.BondsRepository
import ru.dronelektron.investmentcalculator.domain.usecase.CalculateCase

class InvestFormViewModelFactory(
    private val calculateProfit: CalculateCase,
    private val repository: BondsRepository
) : ViewModelProvider.Factory {
    @Suppress("UNCHECKED_CAST")
    override fun <T : ViewModel?> create(modelClass: Class<T>)
    = InvestFormViewModel(calculateProfit,repository) as T
}
