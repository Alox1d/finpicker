package ru.dronelektron.investmentcalculator.presentation.investresult

import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.github.mikephil.charting.animation.Easing
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.data.BarEntry
import com.google.android.material.card.MaterialCardView
import ru.dronelektron.investmentcalculator.R
import ru.dronelektron.investmentcalculator.databinding.FragmentInvestResultBinding
import ru.dronelektron.investmentcalculator.presentation.investform.InvestFormViewModel
import ru.dronelektron.investmentcalculator.presentation.investresult.profitslist.ProfitsAdapter
import ru.dronelektron.investmentcalculator.presentation.slideDown
import ru.dronelektron.investmentcalculator.presentation.slideUp
import ru.dronelektron.investmentcalculator.presentation.toggleArrow
import com.github.mikephil.charting.data.BarData

import com.github.mikephil.charting.interfaces.datasets.IBarDataSet

import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.formatter.DefaultValueFormatter
import com.github.mikephil.charting.formatter.PercentFormatter

import com.github.mikephil.charting.utils.ColorTemplate
import kotlinx.android.synthetic.main.fragment_invest_result.*
import ru.dronelektron.investmentcalculator.domain.BondPercentFormatter


class InvestResultFragment : Fragment() {
    private val investFormViewModel by activityViewModels<InvestFormViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }
    lateinit var banner:MaterialCardView
    lateinit var image:ImageView
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val binding = FragmentInvestResultBinding.inflate(inflater, container, false)

        binding.lifecycleOwner = viewLifecycleOwner
        binding.investFormViewModel = investFormViewModel

        banner = binding.banner
        image = binding.image

        val barChart = binding.barChart

        investFormViewModel.caseResult.observe(viewLifecycleOwner,{
            configBarChart(barChart)
        })

        return binding.root
    }

    private fun configBarChart(barChart: BarChart) {
        val yVals1 = ArrayList<BarEntry>()
        if (investFormViewModel.caseResult.value?.bondValues!!.size>1){
            yVals1.add(

                BarEntry(
                    0f, investFormViewModel.caseResult.value?.bondValues
                )
            )
        }else{
            yVals1.add(

                BarEntry(
                    0f, 1f
                )
            )
        }



        val set1: BarDataSet
        if (barChart.getData() != null &&
            barChart.getData().getDataSetCount() > 0
        ) {
            set1 = barChart.getData().getDataSetByIndex(0) as BarDataSet
            set1.values = yVals1
            barChart.getData().notifyDataChanged()
            barChart.notifyDataSetChanged()
        } else {
            set1 = BarDataSet(yVals1, " ")
            set1.setDrawIcons(false)
//            set1.setDrawValues(false)
            set1.valueTextSize = 22f
            set1.valueFormatter = BondPercentFormatter()
            set1.setColors(getColors(), 1000)
//            set1.stackLabels = arrayOf("Births", "Divorces", "Marriages")
            if (investFormViewModel.caseResult.value?.bondValues!!.size>1){
                set1.stackLabels =investFormViewModel.caseResult.value?.bondNames
                set1.setLabel("")

            }else{
                set1.setLabel(investFormViewModel.caseResult.value?.bondNames?.get(0))
            }

            val dataSets = ArrayList<IBarDataSet>()
            dataSets.add(set1)
            val data = BarData(dataSets)
    //            data.setValueFormatter(MyValueFormatter())
    //            data.setValueTextColor(Color.WHITE)
            barChart.setData(data)
        }
//        barChart.setVisibleXRange(1f,2f)
//        barChart.setFitBars(true)
        barChart.invalidate()
        barChart.axisLeft.setDrawGridLines(false)
        barChart.axisRight.setDrawGridLines(false)
        barChart.xAxis.setDrawGridLines(false)

        barChart.axisLeft.setDrawAxisLine(false)
        barChart.axisRight.setDrawAxisLine(false)
        barChart.xAxis.setDrawAxisLine(false)

        barChart.axisRight.setEnabled(false)
        barChart.axisLeft.setEnabled(false)
        barChart.xAxis.setEnabled(false)
        barChart.description.setEnabled(false)
//        barChart.legend.textSize = 14f
//        barChart.legend.xEntrySpace = 32f
//        barChart.legend.horizontalAlignment = Legend.LegendHorizontalAlignment.LEFT

    }

    private fun getColors(): IntArray {

        // have as many colors as stack-values per entry
        val colors = intArrayOf(
        ColorTemplate.rgb("#006DF0"),
        ColorTemplate.rgb("#0B9ED9"),
        ColorTemplate.rgb("#0CF3F8"),

        )
//        System.arraycopy(ColorTemplate.MATERIAL_COLORS, 0, colors, 0, 3)
        return colors
    }
    override fun onResume() {
//        toggleArrow(banner,true)
//        banner.slideDown(3000)
        // load the animation
        val animFadein = AnimationUtils.loadAnimation(requireContext(),
            R.anim.fade_in);
        banner.startAnimation(animFadein)
        val zoomin = AnimationUtils.loadAnimation(requireContext(),
            R.anim.zoom_in);
        image.startAnimation(zoomin)
        barChart.setData(null)
        barChart.getData()?.notifyDataChanged()
        barChart.invalidate()

        investFormViewModel.caseResult.observe(viewLifecycleOwner,{
//            barChart.getData().dataSets.clear()
            barChart.clear()
            configBarChart(barChart)
            barChart.getData().notifyDataChanged()
            barChart.notifyDataSetChanged()
            barChart.invalidate()

        })
        barChart.animateXY(2500,1500)

        super.onResume()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.menu_invest_result, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.action_chart) {
            findNavController().navigate(R.id.action_invest_result_fragment_to_invest_result_chart_fragment)

            return true
        }

        return super.onOptionsItemSelected(item)
    }
}
