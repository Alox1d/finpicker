package ru.dronelektron.investmentcalculator.presentation

import android.content.Context
import android.view.View
import android.view.animation.Animation
import android.view.animation.TranslateAnimation
import android.view.inputmethod.InputMethodManager
import androidx.fragment.app.Fragment
import android.view.animation.AnimationSet
import kotlin.math.pow
import kotlin.math.roundToInt


fun Fragment.closeKeyboard() {
    activity?.let {
        val view = it.currentFocus ?: return
        val inputMethodManager = it.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager

        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }
}
fun View.slideUp(duration: Int = 500) {
    visibility = View.VISIBLE
    val animate = TranslateAnimation(0f, 0f, this.height.toFloat(), 0f)
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun View.slideDown(duration: Int = 500) {
    visibility = View.VISIBLE
    val animate = TranslateAnimation(0f, 0f, 0f, this.height.toFloat())
    animate.duration = duration.toLong()
    animate.fillAfter = true
    this.startAnimation(animate)
}

fun toggleArrow(view: View, isExpanded: Boolean): Boolean {
    return if (isExpanded) {
        view.animate().scaleY(50f).setDuration(2000).scaleY(1f)
        view.animate().scaleX(50f).setDuration(2000).scaleX(1f)
        view.animate().setDuration(2000).translationY(-100f)
        view.animate().setDuration(500).translationY(0f).start()


        true
    } else {
        view.animate().setDuration(200).rotation(0f)
        false
    }
}


fun Double.roundTo(numFractionDigits: Int): Double {
    val factor = 10.0.pow(numFractionDigits.toDouble())
    return (this * factor).roundToInt() / factor
}