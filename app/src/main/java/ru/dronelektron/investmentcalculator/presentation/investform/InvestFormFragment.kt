package ru.dronelektron.investmentcalculator.presentation.investform

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.AutoCompleteTextView
import android.widget.CheckedTextView
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import ru.dronelektron.investmentcalculator.App
import ru.dronelektron.investmentcalculator.R
import ru.dronelektron.investmentcalculator.databinding.FragmentInvestFormBinding
import ru.dronelektron.investmentcalculator.di.annotation.InvestForm
import ru.dronelektron.investmentcalculator.domain.enums.FieldError
import ru.dronelektron.investmentcalculator.domain.validator.InputFilterMinMax
import ru.dronelektron.investmentcalculator.presentation.closeKeyboard
import javax.inject.Inject

class InvestFormFragment : Fragment() {
    private val investFormViewModel by activityViewModels<InvestFormViewModel> { investFormViewModelFactory }
    private lateinit var binding: FragmentInvestFormBinding

    @Inject
    @InvestForm
    lateinit var investFormViewModelFactory: ViewModelProvider.Factory

    private val balanceErrorObserver = { error: FieldError? ->
        binding.sumLayout.error = when (error) {
            FieldError.EMPTY_FIELD -> getString(R.string.invest_form_error_empty_balance)
            FieldError.ZERO_FIELD -> getString(R.string.invest_form_error_zero_balance)
            else -> null
        }
    }

    private val percentErrorObserver = { error: FieldError? ->
        binding.percentLayout.error = when (error) {
            FieldError.EMPTY_FIELD -> getString(R.string.invest_form_error_empty_percent)
            FieldError.ZERO_FIELD -> getString(R.string.invest_form_error_zero_percent)
            else -> null
        }
    }

    private val iterationsErrorObserver = { error: FieldError? ->
        binding.tilDuration.error = when (error) {
            FieldError.EMPTY_FIELD -> getString(R.string.invest_form_error_empty_iterations)
            FieldError.ZERO_FIELD -> getString(R.string.invest_form_error_zero_iterations)
            else -> null
        }
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)

        (activity?.applicationContext as App).appComponent.inject(this)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentInvestFormBinding.inflate(inflater, container, false)
        binding.lifecycleOwner = viewLifecycleOwner
        binding.investFormViewModel = investFormViewModel

        investFormViewModel.navigateToResultsEvent.observe(viewLifecycleOwner, { event ->
            event.getDataIfNotHandled()?.let {
                closeKeyboard()
                navigateToResults()
            }
        })

        investFormViewModel.sumError.observe(viewLifecycleOwner, balanceErrorObserver)
//        investFormViewModel.percentError.observe(viewLifecycleOwner, percentErrorObserver)
        investFormViewModel.durationError.observe(viewLifecycleOwner, iterationsErrorObserver)
        investFormViewModel.wallets.observe(viewLifecycleOwner, { items:List<String> ->

            val adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
            (binding.menuWallets.editText as? AutoCompleteTextView)?.setAdapter(adapter)
            (binding.menuWallets.editText as? AutoCompleteTextView)?.setText(
                investFormViewModel.wallet.value, false
            )
        })


        binding.tilDuration.editText?.filters = arrayOf(InputFilterMinMax(1, 60))

//        var items = enumValues<Wallet>().map { it.name }
//        adapter = ArrayAdapter(requireContext(), R.layout.list_item, items)
//        (binding.menuWallets.editText as? AutoCompleteTextView)?.setAdapter(adapter)
//            (binding.menuWallets.editText as? AutoCompleteTextView)?.setText(
//                adapter.getItem(0).toString(), false
//            );
        investFormViewModel.bondList.observe(viewLifecycleOwner, Observer {
//            adapter.clear()
//            adapter.addAll(it.securities.data
//                .map { it[0].toString() })
//            (binding.menuWallets.editText as? AutoCompleteTextView)?.setText(
//                adapter.getItem(0).toString(), false
//            );
//            investFormViewModel.formResult(response.body())
        })
//        investFormViewModel.getAllBonds()


        val textView: CheckedTextView = binding.checkRefinance
        textView.setOnClickListener {
            textView.isChecked = !textView.isChecked
            binding.tilAddMontly.isEnabled = textView.isChecked

        }
        binding.tilDuration.editText?.doOnTextChanged { text, start, before, count ->
            if (text?.isNotEmpty() == true && text.toString().toInt() <=36){
                binding.switchIis.isChecked=false
                binding.switchIis.isEnabled=false

            }else{
                binding.switchIis.isEnabled=true

            }
        }



        return binding.root
    }

    private fun navigateToResults() {
        findNavController().navigate(R.id.action_invest_form_fragment_to_invest_result_fragment)
    }
}
