package ru.dronelektron.investmentcalculator.data

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import ru.dronelektron.investmentcalculator.domain.model.BondCollection

interface RetrofitService {
    @GET("iss/engines/stock/markets/bonds/boards/TQOB/securities.json")
    fun getAllSecurities(): Call<BondCollection>

    companion object {

        var retrofitService: RetrofitService? = null

        fun getInstance() : RetrofitService {

            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://iss.moex.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(RetrofitService::class.java)
            }
            return retrofitService!!
        }
    }
}